# I am a little uncertain from the description whether kgco2 should be int or float

class TranslateTreeResponse:

    def __init__(self) -> None:
        self.type: str = ""
        self.description: str = ""


def translate_tree_response(kgco2e: float) -> int:
    # Keep in mind that this will round down for cases of for instance 1.5 -> 1
    area = round(kgco2e/0.049)
    return area
    