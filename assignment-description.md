**Assignment Solution**

In this assignment, I have chosen to use Python.  This was chosen simply because it is a language
I am familiar with.  Depending on how large and computationally demanding the project is, this
might not be an ideal choice, but given the simple equation

```
area = kgco2e/0.049
```

Python should be sufficient when it comes to efficiency, and it is therefore not necessary to rather choose for
instance C++.  There might, however, be good reasons to choose languages that are of a completely
different type, such as JavaScript or TypeScript.  As I am not familiar with these languages, however,
these seemed like poor choices given the time restrictions.

When it comes to security, I note that the yaml.full_load is reported to be risky in the [Documentation I found
to solve the error encountered when using yaml.load](https://github.com/yaml/pyyaml/wiki/PyYAML-yaml.load(input)-Deprecation) (commit `e2d7015b14f82ee5512477622be49f05a3f9a8eb`).  This function should only be used when you trust the content of the file being loaded. 
In this case I did, given that the content came from Ducky, but in the theoretical example that the (not nearly)
implemented API should be used for real purposes, a better loading function should be found.

I will now briefly explain my implementation steps.

As I have never done API programming or encountered .yml files before (except in the case of setting up a GitLab pipeline), my first step was to do a Google search. 
I looked into the [OpenApi Specification](https://swagger.io/specification/), as well as a [Blog post on the subject](https://blog.apideck.com/introduction-to-openapi-specification).  I was in particular interested in
guidelines for how to import content from yml files into Python objects. In this investigation, I encountered
a [StackOverflow page](https://stackoverflow.com/questions/22268952/what-is-the-difference-between-yaml-and-yml-extension#:~:text=yml%20has%20more%20pages%20than,extension%20instead%20of%20the%20correct%20.), stating that .yaml files were preferred over .yml.  I noted this,
but did not take the time to consider whether it might be worthwile to translate the provided .yml file into a
.yaml file.

Quite some time had already passed at this point, and I decided to get started on some coding.  The first step was
to create this GitLab repository, and install a Python extension in VSCode (the editor I chose to use).  I then started to look into how one might read from a .yml file.  At first I tried to use a library called [openapi-parser](https://pypi.org/project/openapi-parser/) but did not quite figure out how it worked, and instead settled for using the yaml library, as described in [in this StackOverflo post](https://stackoverflow.com/questions/61874631/how-to-retrieve-data-from-api-spec-yaml-file-in-python).  In this way I was at least able to extract and print some information from the provided .yml file.

I have to admit that I was not quite sure what implementing an OpenAPI specification means.  My interpretation was that it probably meant extracting information from the .yml file, and from there perform the translation from kg CO2 to required area of trees.  I was not sure, however, whether the actual translation funtion should be read from the file, or explicitly implemented.  I decided to go with the latter, since in this way I would be able to (if nothing else) provide a basis for unittests, and thereby a safe startpoint for subsequent refactorisation if the former was indeed the case.  I did this through a TDD approach, and have tried to commit frequently enough to show the steps in which this was done: first by writing a test that I knew whould fail, then implementing the code to make it pass, and then (though maybe not so much in these simple cases) refactor.  A pipeline file (.gitlab-ci.yml) was added so that pytest would be run to verify the changes through unittests for each new commit. In a real-life project I would have added steps to also check code quality as well as integration tests.

Towards the end, I started to think that maybe both TranslateInput and TranslateTreeResponse should be classes, containing values (for instance kg CO2 and area of trees), as well as constructors, validity checks of values and the translation function itself.  I therefore wrote a placeholder class for TranslateTreeResponse, though it is at this stage only a small part of its scheleton.  

A question I did not end up tackling is where the actual values would come from.  I suppose they would originate from a communcation with some external entity, for instance a web page, database or prompt in the terminal (as is the point of an API).  I did however, not have time to think too much about this.  I must confess that I have in fact used 2.5 hours on this assignment, not two, since I ended up spending rather more time on documenting my thought process than I had anticipated. 