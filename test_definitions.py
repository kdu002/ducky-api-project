from definitions import translate_tree_response, TranslateTreeResponse

def test_translate_tree_response() -> None:

    kgco2e = 0
    area = translate_tree_response(kgco2e)

    assert 0 == area

    kgco2 = 0.049
    area = translate_tree_response(kgco2)

    assert 1 == area

    kgco2 = 0.1
    area = translate_tree_response(kgco2)

    assert 2 == area


def test_TranslateTreeResponse() -> None:
    tree = TranslateTreeResponse()
    assert "" == tree.type
    assert "" == tree.description

